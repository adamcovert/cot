$(document).ready(function () {

  $(window).scroll(function () {
    windowScroll();
  }).scroll();

  function windowScroll() {
    var pageScrollY = 0;
    if (typeof(window.pageYOffset) == 'number') pageScrollY = window.pageYOffset;
    else pageScrollY = document.documentElement.scrollTop;

    if (pageScrollY > 1000) {
      $('.s-page').addClass('s-page--is-scrolling');
    } else {
      $('.s-page').removeClass('s-page--is-scrolling');
    }
  }

  var partnersSlider = new Swiper('.s-partners__slider .swiper-container', {
    loop: true,
    speed: 750,
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 30,
      }
    }
  });


  $('.s-gallery').lightGallery({
    selector: '.s-gallery__thumb',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  $('.s-contacts__certificates').lightGallery({
    selector: '.s-contacts__certificate',
    mode: 'lg-fade',
    cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });

  svg4everybody();

  $('.s-testimonial-item__more-btn').on('click', function () {
    var button = $(this);
    var text = button.text() == 'Читать весь отзыв' ? 'Скрыть' : 'Читать весь отзыв';
    button.text(text);
  });


  /**
   * Show and close contacts menu in mobile
   */
  $('.s-page-header__mobile-actions-btn--contacts').on('click', function () {
    $('.s-mobile-contacts').addClass('s-mobile-contacts--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-contacts__close').on('click', function () {
    $('.s-mobile-contacts').removeClass('s-mobile-contacts--is-active');
    $('body').removeClass('s-prevent-scroll');
  });

  /**
   * Show and close mobile menu in mobile
   */

  $('.s-page-header__mobile-actions-btn--menu').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
    $('body').removeClass('s-prevent-scroll');
  });



  var promoSlider = new Swiper('.s-promo__slider .swiper-container', {
    slidesPerView: 1,
    navigation: {
      nextEl: '.s-promo__slider-button-next',
      prevEl: '.s-promo__slider-button-prev',
    }
  });

  var testimonialsSlider = new Swiper('.s-testimonials__slider .swiper-container', {
    slidesPerView: 2,
    navigation: {
      nextEl: '.s-testimonials__slider-button-next',
      prevEl: '.s-testimonials__slider-button-prev',
    },
    breakpoints: {
      320: {
        spaceBetween: 15
      },
      768: {
        spaceBetween: 30
      }
    }
  });

  var teamSliderThumbs = new Swiper('.s-team__slider-thumbs-swiper', {
    breakpoints: {
      320: {
        direction: 'horizontal',
        slidesPerView: 2,
        spaceBetween: 15,
        centeredSlides: true,
        touchRatio: 0.2,
        slideToClickedSlide: true,
      },
      576: {
        direction: 'horizontal',
        slidesPerView: 3,
        spaceBetween: 10,
      },
      992: {
        direction: 'vertical',
        slidesPerView: 2,
        spaceBetween: 20,
        navigation: {
          nextEl: '.s-team__slider-button-next',
          prevEl: '.s-team__slider-button-prev',
        },
      }
    }
  });

  var teamSliderTop = new Swiper('.s-team__slider-top-swiper', {
    breakpoints: {
      320: {
        direction: 'horizontal'
      },
      992: {
        direction: 'vertical',
        spaceBetween: 20,
      }
    },
    thumbs: {
      swiper: teamSliderThumbs
    }
  });
});